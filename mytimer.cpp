#include "mytimer.h"
#include <QDebug>

MyTimer::MyTimer(Ui::Widget *ui)
{
    // create a timer
    timer = new QTimer(this);

    // setup signal and slot
    connect(timer, SIGNAL(timeout()),
          this, SLOT(MyTimerSlot(*ui)));

    // msec
    timer->start(1000);
}

void MyTimer::MyTimerSlot(Ui::Widget *ui)
{

}
