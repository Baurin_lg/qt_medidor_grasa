#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include "dudata.h"



namespace Ui {
class Widget;
}

class QVBoxLayout;
class QCustomPlot;
class DuData;


class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
private slots:
    void updateCaption();
    void tomarMuestra();
    void dejarTomarMuestra();
    void updatePlot();

    void on_label_seppelec_linkActivated(const QString &link);

private:
    Ui::Widget *ui;
    QTimer *timer;

    QCustomPlot *mPlot;

    DuData *mSenoX;
    DuData *mCosenoX;
    DuData *duprueba;
    DuData *real_time;

    float time_count;

    void calculaValoresSenoX();
    void calculaValoresCosenoX();
    void calculaValoresRaizCuadradaX();

    QVector<double> mX;
    QVector<double> mY;
    float count_time;

};

#endif // WIDGET_H
