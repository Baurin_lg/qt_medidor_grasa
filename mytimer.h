#ifndef MYTIMER_H
#define MYTIMER_H

#include <QTimer>
#include <QWidget>

namespace Ui {
class Widget;
}

class MyTimer : public QObject
{
    Q_OBJECT
public:
    MyTimer(Ui::Widget *ui);
    QTimer *timer;

public slots:
    void MyTimerSlot(Ui::Widget *ui);
};

#endif // MYTIMER_H
