#include "dudata.h"
#include "qcustomplot.h"

DuData::DuData(QCustomPlot *parent)

{
   DuData::mParent = parent;

   // Connector
   QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
   db.setHostName("localhost");
   db.setDatabaseName("sepp_control");
   db.setUserName("root");
   db.setPassword("rstuvw");
   qDebug()<<db.open();
   bool connected = db.open();
   if( !db.open() )
   {
     qDebug() <<QSqlDatabase::drivers() ;
     qFatal( "Failed to connect." );
   }

}



void DuData::drawGraph(){
    mGraph = mParent->addGraph();
    mGraph->setData(mX, mY);

}

float DuData::getSample(){



    float mg = 0.0;
    QSqlQuery query("SELECT * FROM samples order by fechaCreacionMuestra DESC limit 1", db);

    while (query.next()){

//        mg = query.value("mg").toFloat(); // output all names

    }

    return mg;
}

void DuData::getNewMuestra(){

    // Take Value From Samples
    float mg = 0.0;
    float temperatura = 0.0;
    float valorInstrumento = 0;

    QSqlQuery query("SELECT mg, temperatura, valorInstrumento FROM samples order by fechaCreacionMuestra DESC limit 1", db);

    while (query.next()){

//        mg = query.value("mg").toFloat();
//        temperatura = query.value("temperatura").toFloat();
//        valorInstrumento = query.value("valorInstrumento").toFloat();

    }
    query.finish();

    // Insert

    QSqlQuery query2;
//    QString query3 = "INSERT INTO muestrasgrasa (mg, temperatura, valorInstrumento, for_train, seleccionada, rechazado) VALUES (1, 1, 1, 1, 1, 1)";
//    query2.prepare(query3);

    query2.prepare("INSERT INTO muestrasgrasa (mg, temperatura, valorInstrumento, for_train, seleccionada, rechazado)"
                   "VALUES (:mg, :temperatura, :valorInstrumento, 1, 1, 1)");
    query2.bindValue(":mg", mg);
    query2.bindValue(":temperatura", temperatura);
    query2.bindValue(":valorInstrumento", valorInstrumento);

    if (!query2.exec())
        qDebug() << query2.lastError();

    qDebug() << query2.executedQuery();

    db.commit();

    query2.finish();



}

QString DuData::getGroup(){



    QString name = "";
    QSqlQuery query("SELECT * FROM muestrasgrasa_group WHERE selected = 1;", db);

    while (query.next()){

        name = query.value("name").toString();
        qDebug() << name;
    }

    return name;
}

int DuData::getLastId(){



    int new_id = 0;
    QSqlQuery query("SELECT idMuestra FROM muestrasgrasa ORDER BY fechaCreacionMuestra DESC limit 1", db);

    while (query.next()){

        new_id = query.value("idMuestra").toInt() + 1;

    }

    return new_id;
}


