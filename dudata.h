#ifndef DUDATA_H
#define DUDATA_H

#include "QVector"
#include "QObject"

#include <QList>
#include <QString>

#include <QSqlDatabase>


#include <QSqlQuery>


//#include <QSql>
#include <QSqlDriver>
#include <QMessageBox>
#include <QSqlError>


class QCPGraph;
class QCustomPlot;


class DuData : public QObject
{

public:
    DuData(QCustomPlot *parent);

    void setX(const QVector<double> x){

        mX = x;
        drawGraph();
    }

    void setY(const QVector<double> y){

        mY = y;
        drawGraph();
    }

    void prueba(QVector<double> x){
        drawGraph();

    }

    void drawGraph();

    float getSample();
    QString getGroup();
    int getLastId();
    void getNewMuestra();

    bool connected;

private:


    QCPGraph *mGraph;
    QVector<double> mX;
    QVector<double> mY;

    QCustomPlot *mParent;
    QSqlDatabase db;






};

#endif // DUDATA_H
