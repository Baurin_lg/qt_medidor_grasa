#include "widget.h"
#include "ui_widget.h"
#include <iostream>
#include <QTimer>
#include <QDebug>
#include <QObject>
#include "qcustomplot.h"
#include <stdlib.h>     /* srand, rand */

#define XMIN -10.0
#define XMAX 10.0
#define YMIN 0.0
#define YMAX 10.0


Widget::Widget(QWidget *parent) :

    QWidget(parent),
    ui(new Ui::Widget)
{


    mPlot = new QCustomPlot(this);

    ui->setupUi(this);


    mPlot = new QCustomPlot(this);

    // Setting Initial
    ui->verticalLayout_plot->addWidget(mPlot);
    real_time = new DuData(mPlot);
    ui->label_grupo->setText(real_time->getGroup());
    ui->lcdNumber_idmuestra->display(real_time->getLastId());
    real_time->drawGraph();

    ui->lcdNumber_mg->setDigitCount(4);
    ui->lcdNumber_mg->setSmallDecimalPoint(true);



    mPlot->xAxis->setRange(0.0,10.0);
    mPlot->yAxis->setRange(0.0,10.0);


    ui->progressBar_tiempo_espera->reset();
    ui->label_errors->setText("Puede tomar una nueva muestra");
    //ui->label_seppelec->setPixmap(QPixmap("./logo_seppelec_resize.png"));

    count_time = 0;

    for (double iX= 0; iX < XMAX; iX += 0.1){
            mX << iX;
            mY << rand() % 11 ;
    }

    // END SETTING INITIAL

    // Timers

    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updatePlot()));
    timer->start(0.1);

    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateCaption()));
    timer->start(50);

    // END TIMERS

    // Buttons

    connect(ui->pushButton_tomar_muestra, SIGNAL (pressed()),this, SLOT (tomarMuestra()));
    connect(ui->pushButton_tomar_muestra, SIGNAL (released()),this, SLOT (dejarTomarMuestra()));

    connect(ui->pushButton_exit, SIGNAL (released()),this, SLOT (close()));

    // END BUTTONS

}


Widget::~Widget()
{
    delete ui;
}

void Widget::updateCaption(){


    int value_tiempo_espera = ui->progressBar_tiempo_espera->value();
    if (value_tiempo_espera < 100 ){

        ui->progressBar_tiempo_espera->setValue(value_tiempo_espera+1);
    }else{
        ui->label_errors->setText("Puede tomar una nueva muestra");
    }
}

void Widget::updatePlot(){

    count_time = count_time + 0.1;

    // mX.pop_back();
    mY.pop_back();

    float mg_sim = real_time->getSample();

    mY.push_front(float(mg_sim));
    ui->lcdNumber_mg->display(mg_sim);

    mPlot->graph(0)->setData(mX, mY);
    mPlot->replot();

}



void Widget::tomarMuestra(){
    int value_tiempo_espera = ui->progressBar_tiempo_espera->value();
    if (value_tiempo_espera >= 100 ){

        qDebug() << "Tomando Muestra";
        ui->label_errors->setText("Tomando Muestra");
    }else{

        qDebug() << "No ha pasado el tiempo de espera";
        ui->label_errors->setText("No ha pasado el tiempo de espera");
    }

}

void Widget::dejarTomarMuestra(){

    int value_tiempo_espera = ui->progressBar_tiempo_espera->value();
    if (value_tiempo_espera >= 100 ){

        qDebug() << "Parando Muestra";
        QString message = "Muestra tomada con id = ";
        message.append(QString::number(real_time->getLastId()-1));
        message.append("\nDebe esperar para tomar la siguiente.");
        ui->label_errors->setText(message);
        real_time->getNewMuestra();
        ui->progressBar_tiempo_espera->setValue(0);
        ui->lcdNumber_idmuestra->display(real_time->getLastId());

    }else{

        qDebug() << "No se ha podido tomar la muestra";
        ui->label_errors->setText("Aún debe esperar para tomar \n una nueva muestra");
    }

}

void Widget::calculaValoresSenoX(){

//    QVector<double> x;
//    QVector<double> y;

//    for (double iX= XMIN; iX < XMAX; iX += 0.1){
//        x << iX;
//        y << qTan(iX);
//    }



//    mSenoX->setX(x);
//    mSenoX->setY(y);

}
void Widget::calculaValoresCosenoX(){

//    QVector<double> x;
//    QVector<double> y;

//    for (double iX= XMIN; iX < XMAX; iX += 0.1){
//        x << iX;
//        y << qCos(iX);
//    }

//    mCosenoX->setX(x);
//    mCosenoX->setY(y);
}

void Widget::on_label_seppelec_linkActivated(const QString &link)
{

}
